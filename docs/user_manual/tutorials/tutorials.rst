.. _user_tutorials:

*********
Tutorials
*********

The following are step-by-step examples to get you started using Cytoflow.
They include both some basic tutorials as well as examples of some 
more advanced analyses.

**Easy tutorials**

.. toctree::
   :maxdepth: 2
   
   quickstart
   dose_response
   
   
**Advanced tutorials**
   
.. toctree::
   :maxdepth: 2
   
   induction
   
..
  Basic
  -----
  Quick start
  Basic Statistics
  Yeast dose-response
  
  
..
  Advanced
  --------
  2D dose-response (yeast)
  Machine learning
  TASBE
  Advanced statistics (Kiani)
  
  
